package com.example.hp10.tahsahapp

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.android.gms.tasks.OnFailureListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_sign_up.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        bLogin.setOnClickListener({
            loginClickAction()
        })

        tSignup.setOnClickListener({
            var intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        })

    }

    fun loginClickAction(){
        eEmail.error = null
        ePassword.error = null

        if(validation()) {
        var mAuth: FirebaseAuth = FirebaseAuth.getInstance()
        mAuth.signInWithEmailAndPassword(eEmail.text.toString(),ePassword.text.toString()).addOnCompleteListener(this) {
            task ->
                if(task.isSuccessful) {
                    Toast.makeText(this, eEmail.text.toString(), Toast.LENGTH_SHORT).show()
                    var intent = Intent(this, HomeActivity::class.java)
                    startActivity(intent)
                } else {
                    Toast.makeText(this, "Not Successful", Toast.LENGTH_SHORT).show()

                }
        }

//            //                        var user: FirebaseUser?= mAuth.getCurrentUser()
//            هاي بتحطيها بالصفحة الي بعد ال Login
//            عشان نشتييكي اذا ايوزر Logged in ولا لا او فيي يوزر ولا لا
//            و منها بتقدري تجيبي معلوماتو من ال DB
//            فهمتي علي ؟



//        if(validation()){
//            var mAuth: FirebaseAuth = FirebaseAuth.getInstance();
//            mAuth.signInWithEmailAndPassword(eEmail.text.toString(), ePassword.text.toString()).addOnCompleteListener(this)
//            { it ->
//                    if (it.isSuccessful) {
//                        var user: FirebaseUser?= mAuth.getCurrentUser()
//
//                        Toast.makeText(this, eEmail.text.toString(), Toast.LENGTH_SHORT).show()
//                        var intent = Intent(this, HomeActivity::class.java)
//                        startActivity(intent)
//                    } else {
//                    }
//
//            }.addOnFailureListener(this, OnFailureListener {
//                Toast.makeText(this, it.message + eEmail.text.toString(), Toast.LENGTH_SHORT).show()
//            })
            }//if

    }

    fun validation():Boolean{

        var email = eEmail.text.toString()
        var pass = ePassword.text.toString()
        var validation:Boolean = true

        if(email.isEmpty()){
            eEmail.error = "Please check your e-mail"
            validation = false
        }
        if(pass.isEmpty()){
            ePassword.error = "Please check your password"
            validation = false
        }
return validation
    }
}
